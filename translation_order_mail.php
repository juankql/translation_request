<?php
/*
Plugin Name: Translation order
Plugin URI: https://www.facebook.com/vitralsolutions
Description: Dieses Plugin ermöglicht es Ihnen, eine Anwendung der Übersetzung von einer Sprache in eine andere anzuzeigen , die Auswahl der Basissprache , die Sprache, in die Sie übersetzen möchten , und geben eine elektronische Kontaktadresse und auch die gesamte Datei laden zu übersetzen oder schreiben Sie den Text, den Sie möchten, übersetzen.
Version: 1.0.0.RC
Author : Ing. Juan Carlos Quevedo Lussón
Author URI: https://www.facebook.com/vitralsolutions
License: tipo la licencia , por ejemplo, GPL2
*/
function vsTranslationFrontendForm($atributes){
    $idiomas = get_option('translate_languajes');
    $array_idiomas= explode('-',$idiomas);
    $titulo = get_option('titulo_form');
    $subtitulo = get_option('subtitulo_form');
    $email_owner = get_option('email_owner');
    include('form.php');
}
add_shortcode('form_translation_order', 'vsTranslationFrontendForm'); 

function addMyScript() {
    wp_register_script( 'bootstrap-datepicker', plugins_url().'/translation_order/js/bootstrap-datepicker.js', array( 'jquery' ) );
    wp_enqueue_script( 'bootstrap-datepicker' );
    
    wp_enqueue_style('jcqviewstyles', plugins_url().'/translation_order/style.css');
    wp_register_style('bootstrap', plugins_url().'/translation_order/css/bootstrap.min.css');
    
    wp_register_style('font-awesome', get_template_directory_uri().'/css/font-awesome.min.css');
    wp_register_style('datepicker3', get_template_directory_uri().'/css/datepicker3.css');
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('font-awesome');
    wp_enqueue_style('datepicker3'); 
    wp_enqueue_style('bootstrap-select'); 
}
add_action( 'wp_enqueue_scripts', 'addMyScript' );  

function jcqViewSettings()
 {
    // Verificamos si el usuario puede administrar opciones, en caso de que no pueda
    // se envía un mensaje que no tiene los permisos suficientes
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }

    echo '<div class="wrap">';
    echo '<h2 style="text-align:center;padding-bottom:15px;">Translation Order Email Plugin -Einstellungen</h2>';
     echo '<form action="options.php" method="post">';
        settings_fields('translateorder-settings');
        do_settings_sections('translateorder-settings');
    echo '<div style="width:50%;float:left;clear:left;text-align:right;padding-top:5px;">
            <label for="titulo_form" style="padding-right:15px">Titel für die Form</label>
          </div>';
    echo '<div style="width:50%;float:left;clear:none;text-align:left;">
            <input type="text" name="titulo_form" id="titulo_form" placeholder="" style="width:80%;" value="'.get_option('titulo_form').'">
          </div>';
    echo '<div style="width:50%;float:left;clear:left;text-align:right;padding-top:5px;">
            <label for="subtitulo_form" style="padding-right:15px">Untertitel für die Form</label>
          </div>';
    echo '<div style="width:50%;float:left;clear:none;text-align:left;">
            <input type="text" name="subtitulo_form" id="subtitulo_form" placeholder="" style="width:80%;" value="'.get_option('subtitulo_form').'">
          </div>';
    echo '<div style="width:50%;float:left;clear:left;text-align:right;padding-top:5px;">
            <label for="idiomas" style="padding-right:15px">Sprachen <span style="color:#3E11EE;">getrennt durch - (beispiel: Spanisch -Englisch)</span></label>
          </div>';
    echo '<div style="width:50%;float:left;clear:none;text-align:left;">
            <input type="text" name="translate_languajes" id="translate_languajes" placeholder="" style="width:80%;" value="'.get_option('translate_languajes').'">
         </div>';
    echo '<div style="width:50%;float:left;clear:left;text-align:right;padding-top:5px;">
            <label for="email" style="padding-right:15px">E-Mail , an die Anforderungen gesendet</label>
         </div>';
    echo '<div style="width:50%;float:left;clear:none;text-align:left;">
            <input type="email" name="email_owner" id="email_owner" placeholder="" style="width:80%;" value="'.get_option('email_owner').'">
          </div>';
    echo '<div style="text-align:center;padding-left:50%;">';submit_button();echo '</div>';
    echo '</form></div>';
 }
 

function jcqViewMenu() {
     add_options_page( 'Configuración del plugin Translation Order Email', 'Translation Order Email', 'manage_options', 'translateorder-settings', 'jcqViewSettings' );
 }

add_action( 'admin_menu', 'jcqViewMenu' );
function jcqViewRegisterSettings()
 {
    register_setting( 'translateorder-settings', 'titulo_form' );
    register_setting('translateorder-settings', 'subtitulo_form');
    register_setting( 'translateorder-settings', 'translate_languajes' );
    register_setting( 'translateorder-settings', 'email_owner' );
 }

 add_action( 'admin_init', 'jcqViewRegisterSettings');  
 ?>