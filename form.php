<?php 
    $idiomas_form  = (isset($array_idiomas)) ? $array_idiomas : array('Spanisch','Deutsch','Englisch','Französisch');
    $titulo_form = (isset($titulo)) ? $titulo : 'Übersetzungsanfrage';
    $subtitulo_form =(isset($subtitulo))? $subtitulo:'Bitte f';
    $email_form   = (isset($email_owner)) ? $email_owner : 'juankql@gmail.com';
    
?>
<script type="text/javascript" src="<?php echo plugins_url().'/translation_order/js/bootstrap-select.js';?>" ></script> 
<script type="text/javascript" src="<?php echo plugins_url().'/translation_order/js/bootstrap.js';?>" ></script> 
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url()."/translation_order/style.css"?>" media="all"/> 
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url()."/translation_order/css/datepicker3.css"?>" media="all"/> 
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url()."/translation_order/css/bootstrap-select.css"?>" media="all"/> 
<script type="text/javascript" src="<?php echo plugins_url().'/translation_order/js/jquery.validate.js';?>"  ></script> 
<script type="text/javascript" src="<?php echo plugins_url().'/translation_order/js/upload.js';?>"></script>   
<script type="text/javascript" src="<?php echo plugins_url().'/translation_order/js/bootstrap-datepicker.js';?>" ></script>


<div class="row">
    <div class="row" style="text-align:center;"><h2><?php echo $titulo_form;?></h2></div>
    <div class="row" style="text-align:center;margin-top:-25px !important;margin-bottom:15px;"><h4><b><?php echo $subtitulo_form;?></b></h4></div> 
    <div class="row formulario">
        <form id="formcliente" name="formcliente" method="post" enctype="multipart/form-data">
            <input type="hidden" name="email_owner" id="email_owner" value="<?php echo $email_form; ?>">
            <input type="hidden" name="idiomas" id="idiomas" value="<?php echo $idiomas; ?>">
            <div class="row" id="parte_one"> 
                <div class="row">
                    <div class="column col-xs-4 col-md-4 col-lg-4 grupo" style="padding-left: 5%;padding-right: 5%; float:left;text-align: center;">
                        <div class="row">
                            <div class="span"><span style="text-align:center;">* Ausgangssprache:</span></div>
                            <select class="selectpicker" id="idioma_inicial" name="idioma_inicial" data-style="form_component">
                                <?php foreach($idiomas_form as $key=>$idioma):?>
                                    <option value="<?php echo $key;?>"><?php echo $idioma;?></option>
                                <?php endforeach;?>    
                            </select>     
                        </div>
                        <div class="row icono">     
                           <i class="glyphicon glyphicon-circle-arrow-down"></i>
                        </div>
                        <div class="row" >
                            <div class="span"><span  style="text-align:center;">* Zielsprache:</span></div>
                            <select class="selectpicker"  id="idioma_final" name="idioma_final" data-style="form_component" placeholder="Nachricht">
                                <?php foreach($idiomas_form as $key=>$idioma):?>
                                    <option value="<?php echo $key;?>"><?php echo $idioma;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="row">
                            <div class="mensaje" id="mensaje2"></div>
                        </div>
                    </div>
                    <div class="column col-xs-4 col-md-4 col-lg-4 grupo" style="padding-left: 5%;padding-right: 5%; float:left;text-align: center;"> 
                        <div class="row icono" >     
                           <i class="glyphicon glyphicon-folder-open"></i>
                        </div>
                        <div class="row">
                            <div class="span"><span>* Datei Upload:</span></div>
                            <div id="adjuntos">  
                                <input class="form_component" style="width:100%;" name="file_upload[]" type="file" >
                            </div>
                            <span>
                                <a href="#" onClick="addCampo()" style="font-size:14px;padding-bottom:20px !important;">Weitere Datei hochladen</a>
                            </span> 
                        </div>
                        <div class="row">
                            <div class="span"><span>Wortanzahl:</span></div> 
                            <input defaultvalue="" class="form_component" id="cantidad_palabras" style="width:100%;" name="cantidad_palabras" placeholder="Wortanzahl" type="text"> 
                        </div>
                        <div class="row">
                            <div class="span"><span>Fachgebiet:</span></div> 
                            <input class="form_component" id="tema" defaultvalue="" style="width:100%;" name="tema" placeholder="Fachgebiet" type="text"> 
                        </div>
                    </div>
                    <div class="column col-xs-4 col-md-4 col-lg-4 grupo2" style="padding-left: 5%;padding-right: 5%; float:left;text-align: center;">
                        <div class="row icono">     
                           <i class="glyphicon glyphicon-time"></i>
                        </div>
                        <div class="row input-daterange">
                            <div class="span"><span>Liefertermin:</span></div>
                            <input class="form_component" id="fecha" defaultvalue="" style="width:100%;" name="fecha" placeholder="Liefertermin" type="text"> 
                        </div>
                        <div class="row">
                            <div class="span"><span>Nachricht:</span></div>
                            <textarea class="form_component" defaultvalue="" id="comentarios" style="width:100%;" name="comments" placeholder="Nachricht"></textarea>  
                        </div>
                        <div class="row" style="padding-top:20px;">  
                            <a class="btn btn-success" href="#" id="continuar" style="background-color:#7BC1D8 !important;border-color:#7BC1D8 !important;"><i class="glyphicon glyphicon-share-alt"></i></a>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="row" id="parte_two">
                <div class="row" style="text-align:center;"><h3><b>Kontakt</b></h3></div> 
                <div class="row"> 
                    <div class="column col-xs-6 col-md-6 col-lg-6 grupo3" style="padding-left: 8%;padding-right: 8%; float:left;text-align: center;">
                        <div class="row icono" >     
                           <i class="glyphicon glyphicon-user"></i>
                        </div>    
                        <div class="row">
                            <div class="span"><span>* E-Mail:</span></div>
                            <input defaultvalue="" class="form_component" style="width:100%;" id="email" name="email" placeholder="E-Mail" type="email"> 
                        </div>
                        <div class="row">
                            <div class="span"><span>* Name, Vorname:</span></div> 
                            <input defaultvalue="" class="form_component" style="width:100%;" id="nombre" name="nombre" placeholder="Name, Vorname" type="text"> 
                        </div>
                        <div class="row">
                            <div class="span"><span>Firmenname:</span></div> 
                            <input defaultvalue="" class="form_component" style="width:100%;" id="firma" name="firma" placeholder="Firmenname" type="text"> 
                        </div>
                    </div>
                    <div class="column col-xs-6 col-md-6 col-lg-6 grupo4" style="padding-left: 8%;padding-right: 8%; float:left;text-align: center;"> 
                        <div class="row icono" >     
                            <i class="glyphicon glyphicon-home"></i>
                        </div> 
                        <div class="row">
                            <div class="span"><span>Straße:</span></div> 
                            <input defaultvalue="" class="form_component" style="width:100%;" id="calle" name="calle" placeholder="Straße" type="text"> 
                        </div>
                        <div class="row">
                            <div class="span"><span>PLZ, Stadt:</span></div> 
                            <input defaultvalue="" class="form_component" style="width:100%;" id="codpostal" name="codpostal" placeholder="PLZ, Stadt" type="text"> 
                        </div>
                        <div class="row">
                            <div class="span"><span>Telefonnummer:</span></div> 
                            <input defaultvalue="" class="form_component" id="telefono" style="width:100%;" name="telefono" placeholder="Telefonnummer" type="text"> 
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-top:20px;text-align:center;">   
                    <a class="btn btn-success enviar" href="#" id="continuar2" style="background-color:#7BC1D8 !important;border-color:#ffffff !important;width:20%;height:40px;font-size:16px;padding-top:8px;"><b>Senden</b></a>
                    <div id="mensaje" class="mensaje"></div>
                </div>
            </div>
            
        </form>
    </div>
</div>

<script type="text/javascript">
    jQuery(
        function($){
            var numero = 0; 
            evento = function (evt) { 
               return (!evt) ? event : evt;
            }
            
            addCampo = function () { 
               nDiv = document.createElement('div');
               nDiv.className = '';
               nDiv.id = 'file' + (++numero);
               nCampo = document.createElement('input');
               nCampo.name = 'file_upload[]';
               nCampo.type = 'file';
               nCampo.className ='form_component';
               a = document.createElement('a');
               a.name = nDiv.id;
               a.href = '#';
               a.onclick = elimCamp;
               a.className = 'eliminar_fichero';
               a.innerHTML = 'Datei löschen';
               nDiv.appendChild(nCampo);
               nDiv.appendChild(a);
               container = document.getElementById('adjuntos');
               container.appendChild(nDiv);
            }
            elimCamp = function (evt){
               evt = evento(evt);
               nCampo = rObj(evt);
               div = document.getElementById(nCampo.name);
               div.parentNode.removeChild(div);
            }
            rObj = function (evt) { 
               return evt.srcElement ?  evt.srcElement : evt.target;
            }
            
            function seleccionar()
            {
                var allinputs = document.getElementsByTagName("input");
                var inputs = new Array(); var j = 0; 
                for(i=0; i<allinputs.length; i++)
                {
                    if (allinputs[i].type == "file")
                    {
                        inputs[j] = allinputs[i];
                        j++;
                    }
                }
                return comprobacion(inputs);
            }    

            function comprobacion(iarray)
            {    
                var cantidad=0;
                for(m=0; m < iarray.length; m++) 
                {
                    var cadena1 = iarray[m].value; 
                    if(cadena1!="")
                    {
                        cantidad++;
                        
                    }
                }
                if(cantidad==0)
                {
                    return false;
                }
                return true;
            }
            
            $('#parte_two').hide();
            
            $('#continuar').click( function(){
                var idioma_inicial=$('#idioma_inicial').val();
                var idioma_final=$('#idioma_final').val();
                if(idioma_inicial==idioma_final)
                {
                    $("#mensaje2").html("Ausgangs- und Zielsprache sind identisch. Es wird ein Angebot für ein Lektorat erstellt.");     
                }
                
                $('#parte_two').toggle();
            });
            
            $('#continuar2').click( function(e){
                e.preventDefault();
                $('#formcliente').submit();
            });
            
            $('.input-daterange').datepicker({
                    todayBtn: "linked",
                    todayHighlight: true,
                    autoclose: true,
                    multidate:false,
                    format: "dd/mm/yyyy"
            });
            
              
                
            $("#formcliente").on("submit", function(e){
                e.preventDefault(); 
                var exist_file = seleccionar();
                if(exist_file){
                    
                    var f = $(this);
                
                    var formData = new FormData(document.getElementById("formcliente"));
                    //formData.append("dato", "valor");
                    //formData.append(f.attr("name"), $(this)[0].files[0]);
                    $.ajax({
                        url: "<?php echo plugins_url(); ?>/translation_order/jcqsendmail.php",
                        type: "post",
                        dataType: "html",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false
                    })
                        .done(function(res){
                            $("#mensaje").html(res);
                        });
                }
                else{
                    $("#mensaje").html(" Bitte laden Sie mindestens eine Datei hoch.");
                    return false;      
                }
            });
            
            
        }
        
    );
</script>
                    